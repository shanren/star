package com.hk.star;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedData {
	
	public static void AddData(Context ctx, String key,String val){
		SharedPreferences sp = ctx.getSharedPreferences("SP", 1);
		Editor editor = sp.edit();
		editor.putString(key, val);
		editor.commit();
	}
	
	public static String GetData(Context ctx, String key){
		String val;
		SharedPreferences sp = ctx.getSharedPreferences("SP", 1);
		val= sp.getString(key, "");
		return val;
	}
}
