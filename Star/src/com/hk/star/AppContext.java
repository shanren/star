package com.hk.star;

import com.thinkland.juheapi.common.CommonFun;
import android.app.Application;

public class AppContext extends Application{
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		CommonFun.initialize(getApplicationContext());
	}
}
