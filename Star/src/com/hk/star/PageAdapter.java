package com.hk.star;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PageAdapter extends FragmentPagerAdapter {

	public PageAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {

		if (position == 0) {
			Fragment fragment = new ItemToday();
			return fragment;
		}
		if (position == 1) {
			Fragment fragment = new ItemTomorrow();
			return fragment;
		}
		if (position == 2) {
			Fragment fragment = new ItemWeek();
			return fragment;
		}
		if (position == 3) {
			Fragment fragment = new ItemNextweek();
			return fragment;
		}
		if (position == 4) {
			Fragment fragment = new ItemMonth();
			return fragment;
		}
		if (position == 5) {
			Fragment fragment = new ItemYear();
			return fragment;
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 6;
	}

}
