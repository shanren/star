package com.hk.star;

import org.json.JSONException;
import org.json.JSONObject;

import com.thinkland.juheapi.common.CommonFun;
import com.thinkland.juheapi.common.JsonCallBack;
import com.thinkland.juheapi.data.constellation.ConstellationData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.RatingBar;
import android.widget.TextView;

public final class ItemWeek extends Fragment {

	TextView t_tv_name,t_tv_datetime,t_tv_health,t_tv_love,t_tv_money,t_tv_work;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view=inflater.inflate(R.layout.week, container, false);
		
		t_tv_name= (TextView)view.findViewById(R.id.t_tv_name);
		t_tv_datetime=(TextView)view.findViewById(R.id.t_tv_datetime);
		t_tv_health=(TextView)view.findViewById(R.id.t_tv_health);
		t_tv_love=(TextView)view.findViewById(R.id.t_tv_love);
		t_tv_money=(TextView)view.findViewById(R.id.t_tv_money);
		t_tv_work=(TextView)view.findViewById(R.id.t_tv_work);	
		
		ConstellationData c = ConstellationData.getInstance();
		c.getAll(AppConfig.StarName, "week", new JsonCallBack() {

			@Override
			public void jsonLoaded(JSONObject j) {
				try {
					Log.i("star,week", j.toString());
					t_tv_name.setText(j.getString("name"));
					t_tv_datetime.setText(j.getString("date"));
					t_tv_health.setText(j.getString("health"));
					t_tv_love.setText(j.getString("love"));
					t_tv_money.setText(j.getString("money"));
					t_tv_work.setText(j.getString("work"));
				} catch (JSONException e) {
					e.printStackTrace();
				}			
			}
		});
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

}
