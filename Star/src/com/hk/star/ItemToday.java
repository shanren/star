package com.hk.star;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.thinkland.juheapi.common.CommonFun;
import com.thinkland.juheapi.common.JsonCallBack;
import com.thinkland.juheapi.data.constellation.ConstellationData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.RatingBar;
import android.widget.TextView;

public final class ItemToday extends Fragment {

	TextView t_tv_name,t_tv_datetime,t_tv_number,t_tv_QFriend,t_tv_summary,t_tv_color;
	RatingBar t_rb_all,t_rb_health,t_rb_love,t_rb_money,t_rb_work;
	View view;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view=inflater.inflate(R.layout.today, container, false);
		
		t_tv_name= (TextView)view.findViewById(R.id.t_tv_name);
		t_tv_datetime=(TextView)view.findViewById(R.id.t_tv_datetime);
		t_tv_QFriend= (TextView)view.findViewById(R.id.t_tv_QFriend);
		t_tv_summary= (TextView)view.findViewById(R.id.t_tv_summary);
		t_tv_color= (TextView)view.findViewById(R.id.t_tv_color);
		t_tv_number= (TextView)view.findViewById(R.id.t_tv_number);
		
		t_rb_all=(RatingBar)view.findViewById(R.id.t_rb_all);
		t_rb_health=(RatingBar)view.findViewById(R.id.t_rb_health);
		t_rb_love=(RatingBar)view.findViewById(R.id.t_rb_love);
		t_rb_money=(RatingBar)view.findViewById(R.id.t_rb_money);
		t_rb_work=(RatingBar)view.findViewById(R.id.t_rb_work);	
		
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
//		String key=AppConfig.StarName+"_"+"today"+"_"+df.format(new Date());
//		String val= SharedData.GetData(view.getContext(), key);
//		if(val.equals("")){
//			
//		}
		
		ConstellationData c = ConstellationData.getInstance();
		c.getAll(AppConfig.StarName, "today", new JsonCallBack() {

			@Override
			public void jsonLoaded(JSONObject j) {
				try {
					Log.i("star,today", j.toString());

					
					t_tv_name.setText(j.getString("name"));
					t_tv_datetime.setText(j.getString("datetime"));
					t_tv_QFriend.setText(j.getString("QFriend"));
					t_tv_summary.setText(j.getString("summary"));
					t_tv_color.setText(j.getString("color"));
					t_tv_number.setText(j.getString("number"));
					
					Float health= Float.valueOf(j.getString("health").replaceAll("%", ""))/100*5;
					t_rb_health.setRating(health);
					
					Float all= Float.valueOf(j.getString("all").replaceAll("%", ""))/100*5;
					t_rb_all.setRating(all);
					
					Float love= Float.valueOf(j.getString("love").replaceAll("%", ""))/100*5;
					t_rb_love.setRating(love);
					
					Float money= Float.valueOf(j.getString("money").replaceAll("%", ""))/100*5;
					t_rb_money.setRating(money);
					
					Float work= Float.valueOf(j.getString("work").replaceAll("%", ""))/100*5;
					t_rb_work.setRating(work);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}			
			}
		});
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

}
