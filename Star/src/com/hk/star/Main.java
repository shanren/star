package com.hk.star;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Main  extends Activity{
	
	LinearLayout btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12;
	boolean isExit=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		findid();
		
		OnClickListener lister=new OnClickListener() {
			@Override
			public void onClick(View v) {
				int btn=v.getId();
				switch(btn){
					case R.id.btn1:
						showDetail("������");
					break;
					case R.id.btn2:
						showDetail("��ţ��");
					break;
					case R.id.btn3:
						showDetail("˫����");
					break;
					case R.id.btn4:
						showDetail("��з��");
					break;
					case R.id.btn5:
						showDetail("ʨ����");
					break;
					case R.id.btn6:
						showDetail("��Ů��");
					break;
					case R.id.btn7:
						showDetail("�����");
					break;
					case R.id.btn8:
						showDetail("��Ы��");
					break;
					case R.id.btn9:
						showDetail("������");
					break;
					case R.id.btn10:
						showDetail("Ħ����");
					break;
					case R.id.btn11:
						showDetail("ˮƿ��");
					break;
					case R.id.btn12:
						showDetail("˫����");
					break;
				}
			}
		};
		
		btn1.setOnClickListener(lister);
		btn2.setOnClickListener(lister);
		btn3.setOnClickListener(lister);
		btn4.setOnClickListener(lister);
		btn5.setOnClickListener(lister);
		btn6.setOnClickListener(lister);
		btn7.setOnClickListener(lister);
		btn8.setOnClickListener(lister);
		btn9.setOnClickListener(lister);
		btn10.setOnClickListener(lister);
		btn11.setOnClickListener(lister);
		btn12.setOnClickListener(lister);
	}
	
	public void findid(){
		btn1 = (LinearLayout) findViewById(R.id.btn1);
		btn2 = (LinearLayout) findViewById(R.id.btn2);
		btn3 = (LinearLayout) findViewById(R.id.btn3);
		btn4 = (LinearLayout) findViewById(R.id.btn4);
		btn5 = (LinearLayout) findViewById(R.id.btn5);
		btn6 = (LinearLayout) findViewById(R.id.btn6);
		btn7 = (LinearLayout) findViewById(R.id.btn7);
		btn8 = (LinearLayout) findViewById(R.id.btn8);
		btn9 = (LinearLayout) findViewById(R.id.btn9);
		btn10 = (LinearLayout) findViewById(R.id.btn10);
		btn11 = (LinearLayout) findViewById(R.id.btn11);
		btn12 = (LinearLayout) findViewById(R.id.btn12);
	}
	
	public void showDetail(String star){
		boolean flg= Utils.checkNetWork(this, true);
		if(flg==true){
			AppConfig.StarName=star;
			Intent intent = new Intent(Main.this, Detail.class);  
			startActivity(intent);
		}
	}
	
	@Override
	public void finish() {
		if (isExit == false) {
			isExit = true;
			Toast.makeText(this, "�ٰ�һ�η��ؼ��˳�", Toast.LENGTH_SHORT).show();
			new Timer().schedule(new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			}, 2500);
		} else {
			Main.super.finish();
		}
	}
}
